# Trace Framework

This is a general-purpose event tracing framework for recording and publishing stacked traces.

Traces can be sent via a socket to a profiling tool.
